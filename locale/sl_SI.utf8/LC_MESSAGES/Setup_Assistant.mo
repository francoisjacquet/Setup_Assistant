��          �      �       H     I     W  	   o     y  
   �     �     �     �     �     �     �     �     �  �       �       
   *     5     >  
   L  "   W     z     �     �  
   �     �     �                               	                    
              Consult my %s Consult the inline Help Create %s Edit %s Edit my %s Enabled Get the Helpful Tips PDF Import Students Print my Handbook Quick Setup Guide Setup %s Setup Assistant Setup Assistant %s Project-Id-Version: Setup Assistant plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 19:22+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SearchPath-0: .
 Posvetujte se z mojim %s Oglejte si vgrajeno pomoč Kreiraj %s Uredi %s Uredi moje %s Omogočeno Pridobite PDF s koristnimi nasveti Uvozi dijaka Natisni moj priročnik Priročnik za hitro namestitev Nastavi %s Pomočnik za nastavitev Pomočnik za konfiguracijo %s 
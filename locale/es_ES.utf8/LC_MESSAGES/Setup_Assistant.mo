��          �      �       H     I     W  	   o     y  
   �     �     �     �     �     �     �     �     �         (     8     U     b     v     �  "   �     �     �     �               4                               	                    
              Consult my %s Consult the inline Help Create %s Edit %s Edit my %s Enabled Get the Helpful Tips PDF Import Students Print my Handbook Quick Setup Guide Setup %s Setup Assistant Setup Assistant %s Project-Id-Version: Setup Assistant plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 19:21+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-Bookmarks: -1,-1,3,-1,-1,-1,-1,-1,-1,-1
X-Poedit-SearchPath-0: .
 Consultar mi %s Consultar la Ayuda en línea Crear los %s Editar la(s)/los %s Editar mi %s Activado Obtener el PDF de Consejos Útiles Importar los Estudiantes Imprimir mi Manual de Usuario Guía de Configuración Rápida Configurar las %s Asistente de Configuración Asistente de Configuración %s 
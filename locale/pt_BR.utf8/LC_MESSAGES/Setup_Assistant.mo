��          �      �       H     I     W  	   o     y  
   �     �     �     �     �     �     �     �     �  �       �     �       	   "     ,  
   :     E     c     s     �     �     �     �                               	                    
              Consult my %s Consult the inline Help Create %s Edit %s Edit my %s Enabled Get the Helpful Tips PDF Import Students Print my Handbook Quick Setup Guide Setup %s Setup Assistant Setup Assistant %s Project-Id-Version: Setup Assistant plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 19:22+0200
Last-Translator: Emerson Barros
Language-Team: 
Language: pt_BR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-Bookmarks: -1,-1,3,-1,-1,-1,-1,-1,-1,-1
X-Poedit-SearchPath-0: .
 Consultar meu %s Consulte a ajuda embutida Criar %s Editar %s Editar meu %s Habilitado Obtenha o PDF de dicas úteis Importar alunos Imprimir meu manual do usuário Guia de configuração rápida Configurar %s Assistente de configuração Assistente de configuração %s 